# Threads & IO Behavior

* OSX 10.8.5
* Ruby 2.0.0p247

## Execution with IRB runs without issue

```
./console
run_threads
```

## Execution with Ruby fails

Threads don't terminate.

```
ruby ./run_threads.rb
```

## Execution with Rake fails

Threads don't terminate.

```
rake run_threads
```
